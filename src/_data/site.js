'use strict'

module.exports = {
  author: 'Chris Dilworth',
  copyrightYear: (new Date()).getFullYear()
}
