const now = String(Date.now())
  
let ejs = require("ejs");

module.exports = function (eleventyConfig) {
  
  eleventyConfig.setLibrary("ejs", ejs); // to use std ejs syntax and not 11ty's

  eleventyConfig.setUseGitIgnore(false)
  
  eleventyConfig.addPassthroughCopy({
    './node_modules/alpinejs/dist/cdn.js': './assets/js/alpine.js',
  })
  
  eleventyConfig.addPassthroughCopy({
    './src/assets/img': './assets/img',
  })

  eleventyConfig.addShortcode('version', function () {
    return now
  })
  
  return {
    
    pathPrefix: "/11tytest",

    markdownTemplateEngine: 'ejs', // to use ejs with md
    dataTemplateEngine: 'ejs',     // to use ejs with data
    htmlTemplateEngine: 'ejs',     // to use ejs with html
  
    dir: {
      input: 'src',
      output: 'public'
    }  
  }
}
